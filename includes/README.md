# MonkeyTheme

### Directory path
framework/includes

### File format
Only "php" files allowed
example: _header.php

### Information
It is important to use an underscore before every file. (ex: _header.php)
