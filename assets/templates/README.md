# MonkeyTheme

### Directory path
framework/assets/templates

### File format
Only "php" files allowed
example: page.php

### Information
Put here all template files
"includes" folder is for template parts like header and footer. It is important to use an underscore before every file. (ex: _header.php)


Via gulp, all templates are going to be duplicate in the normal root path framework/page.php.
[Here is why the files are going to be duplicated](https://visible.vc/engineering/asset-pipeline-for-wordpress-theme-development/)
