<?php

	/*
	* Adding GULP BrowserSync script
	*/

	function gulp_browser_sync() {
		wp_register_script( 'gulp-browser-sync', WP_HOME.':3060/browser-sync/browser-sync-client.js', false, '2.26.3', true );
		wp_enqueue_script( 'gulp-browser-sync' );
	}

	add_action('wp_footer', 'gulp_browser_sync');