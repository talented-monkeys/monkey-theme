<?php
	/**
	 * Remove unused generated css and js files
	 */

	global $sitepress;

	// Remove obsolete wp_head elements
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'generator');
	remove_action('wp_head', 'favicon');
	remove_action('wp_head', array( $sitepress, 'meta_generator_tag' ) );
	remove_action('wp_head', 'rest_output_link_wp_head',10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links',10);
	remove_action('wp_head', 'wp_resource_hints', 2);

	add_filter('rest_enabled','_return_false');
	add_filter('rest_jsonp_enabled','_return_false'); 

	// Disable block editor for posts
	add_filter('use_block_editor_for_post', '__return_false');

	function remove_generated_css(){
		wp_dequeue_style( 'wp-block-library' );
	}

	function remove_gerated_js(){
	  wp_deregister_script( 'wp-embed' );
	}

	add_action( 'wp_enqueue_scripts', 'remove_generated_css', 100 );
	add_action( 'wp_footer', 'remove_gerated_js' );