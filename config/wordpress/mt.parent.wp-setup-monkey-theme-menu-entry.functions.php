<?php

	/*
	* Adding wordpress menu item for monkey-theme
	*/

	// Monkey Theme about page content
	function fn_monkey_theme_about(){	
		$current_lang = substr(get_user_locale(), 0, strpos(get_user_locale(), "_"));
		include get_template_directory() . "/config/monkey-theme/about/monkey-theme-about-".$current_lang.".view.php";
	}

	// Adding Monkey Theme admin menu item with "about" content
	function add_monkey_theme_top_menu(){
	  $page_title = 'Monkey Theme';
	  $menu_title = 'Monkey Theme';
	  $capability = 'manage_options';
	  $menu_slug  = 'monkey-theme-about';
	  $function   = 'fn_monkey_theme_about';
	  $icon_url   = get_template_directory_uri()."/assets/images/icons/monkey-theme-menu-icon.png";
	  $position   = 4;

	  add_menu_page( $page_title,
	                 $menu_title, 
	                 $capability, 
	                 $menu_slug, 
	                 $function, 
	                 $icon_url, 
	                 $position );
	}

	// Adding Monkey Theme admin menu specific styles
	function add_monkey_theme_menu_styles() {
	  echo '<style>
	    #toplevel_page_monkey-theme-about > a > div.wp-menu-image.dashicons-before > img{
				padding-top: 4px;
			}
	  </style>';
	}
 	
 	// Adding about actions to apply items
	add_action( 'admin_menu', 'add_monkey_theme_top_menu' );
	add_action('admin_head', 'add_monkey_theme_menu_styles');