<?php

	/*
	* Enqeueue assets
	*/

	function theme_parent_styles() {
		$parent_theme_css_uri = get_template_directory_uri() . '/dist/css/combined';
		$parent_theme_css_path = get_template_directory() . '/dist/css/combined';
		$parent_theme_css_cache_version = 'p_'.MT_PARENT_CACHE_VERSION_TIMESTAMP;

		$assetCssFiles = array('preloader', 'atf', 'project', 'modules', 'vendor');

		foreach ($assetCssFiles as $assetCssFile) {
			if(file_exists ( $parent_theme_css_path.'/'.$assetCssFile.'.min.css' )){
				wp_register_style('parent-'.$assetCssFile.'-style',	$parent_theme_css_uri.'/'.$assetCssFile.'.min.css', false, $parent_theme_css_cache_version, 'screen');
				wp_enqueue_style('parent-'.$assetCssFile.'-style');
			} 
		}
	}

	function theme_parent_scripts() {
		$parent_theme_js_uri = get_template_directory_uri() . '/dist/js/combined';
		$parent_theme_js_path = get_template_directory() . '/dist/js/combined';
		$parent_theme_js_cache_version = 'p_'.MT_PARENT_CACHE_VERSION_HASH_SHORT;

		$assetJsFiles = array('preloader', 'atf', 'project', 'modules', 'vendor');

		foreach ($assetJsFiles as $assetJsFile) {
			if(file_exists ( $parent_theme_js_path.'/'.$assetJsFile.'.min.js' )){
				wp_register_script('parent-'.$assetJsFile.'-scripts', $parent_theme_js_uri.'/'.$assetJsFile.'.min.js', false, $parent_theme_js_cache_version, true);
				wp_enqueue_script('parent-'.$assetJsFile.'-scripts');
			} 
		}
	}

	add_action('wp_footer', 'theme_parent_styles');
	add_action('wp_footer', 'theme_parent_scripts');