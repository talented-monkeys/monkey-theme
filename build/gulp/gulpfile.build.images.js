'use strict';

// Shared
const shared = require('./gulpfile.build.shared');

// General
const gulp = require('gulp');
const changed = require('gulp-changed');
const fs = require('file-system');
const shell = require('shelljs');
const plumber = require('gulp-plumber');
const debug = require('gulp-debug');

// Images
const imagemin = require('gulp-imagemin');

const registerTasks = function ({	namespace, source, dist, notifyTimeout } = {}) {

	// Define default config, if no parameters are given
	namespace 		= typeof namespace 			!== 'undefined' ? namespace 	: 'build';
	source 			= typeof source 			!== 'undefined' ? source 		: ['./assets/images']
	dist 			= typeof dist 				!== 'undefined' ? dist 			: './dist/img';
	notifyTimeout 	= typeof notifyTimeout 		!== 'undefined' ? notifyTimeout : 3;

	let imageFiles = [
		source+'/**/*.(png|jpg|gif|svg)',
	];

	gulp.task(namespace+':images:optimize', function() {
		var error = false;
		var errorMessage;

		if (!fs.existsSync(dist)){
			shell.mkdir('-p', dist);
		}

		var stream = gulp.src(imageFiles)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':images:optimize -'}))
		.pipe(changed(dist))
		.pipe(imagemin({
			interlaced: true,
			progressive: true,
			verbose: true
		}))
		.pipe(gulp.dest(dist))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':images:optimize" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':images:optimize',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':images:optimize" done!');
			}
		});

		return stream;
	});
}

let GulpfileBuildImages = function () {
	this.registerTasks = registerTasks;
};

module.exports = new GulpfileBuildImages();
