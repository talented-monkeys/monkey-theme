'use strict';

// Shared & Tasks
const shared = require('./gulpfile.build.shared');
const sassTasks = require('./gulpfile.build.sass');
const javascriptTasks = require('./gulpfile.build.javascript');
const imagesTasks = require('./gulpfile.build.images');
const cacheVersionsTasks = require('./gulpfile.cache.versions');

// General
const gulp = require('gulp');
const browserSync = require('browser-sync').create();;
const reload = browserSync.reload;

const registerTasks = function ( {
	namespace,
	themeDir,
	dist,
	sourcesStyles,
	sourcesScripts,
	sourcesImages
} = {} ) {

	// Define default config, if no parameters are given
	namespace 		= typeof namespace 		!== 'undefined' ? namespace 		: 'build';
	themeDir 		= typeof themeDir 		!== 'undefined' ? themeDir 			: './content/themes/monkey-theme';
	dist 			= typeof dist 			!== 'undefined' ? dist 				: './dist';
	sourcesStyles 	= typeof sourcesStyles 	!== 'undefined' ? sourcesStyles 	: [
																				'./assets/styles/**/*.s+(a|c)ss',
																				'./(layout|modules)/**/*.s+(a|c)ss',
																			];

	sourcesScripts 	= typeof sourcesScripts !== 'undefined' ? sourcesScripts 	: [
																		'./assets/scripts/**/*.(js|ts|coffee)js',
																		'./assets/scripts/**/*.(js|ts|coffee).php',
																		'./(layout|modules)/**/*.(js|ts|coffee)js',
																		'./(layout|modules)/**/*.(js|ts|coffee).php',
																	];

	sourcesImages 	= typeof sourcesImages 	!== 'undefined' ? sourcesImages 	: [
																		'./assets/images/**/*.(png|jpg|gif|svg)',
																		'./(layout|modules)/**/*.(png|jpg|gif|svg)',
																	];

	let sourcesAll = [];
	let browerSyncSource = sourcesAll.concat(sourcesStyles, sourcesScripts, sourcesImages);
	let browserSyncSSL;

	if (shared.browserSyncConfigHttps){
		browserSyncSSL = {
			key: shared.browserSyncConfigHttpsKey,
			cert: shared.browserSyncConfigHttpsCert
		};
	}
	else {
		browserSyncSSL = shared.browserSyncConfigHttps;
	}

	gulp.task(namespace+':watcherNotification', function (done) {
		let stream = shared.devNotification({
			title: '✅ Task '+namespace+':assets:watch"',
			message: "Watcher tasks done. Reloading the page...",
			icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
			image: shared.gulpNotificationIconPath + 'gulp-minify.jpg',
			notificationTimeOut: 5,
			openUrl: '',
			actionsLabel: '✅'
		});

		reload();
		done();
		return stream;
	});

	gulp.task(namespace+':assets:watch', function () {
		browserSync.init({
			logPrefix: shared.browserSyncConfigLogPrefix,
			host: shared.browserSyncConfigHost,
			port: shared.browserSyncConfigPort,
			open: shared.browserSyncConfigOpen,
			notify: shared.browserSyncConfigNotify,
			ghost: shared.browserSyncConfigGhost,
			injectChanges: shared.browserSyncConfigInjectChanges,
			https: browserSyncSSL,
			files: browerSyncSource,
			callbacks: {
				ready: function(err, bs) {
				}
			}
		});

		gulp.watch(sourcesStyles, gulp.series(
			// Generate new cache versions
			namespace+':cache:versions',

			// Sass tasks, sync with gulpfile.build.sass.js
			namespace+':clean',
			namespace+':sass:generate',
			namespace+':lint:sass',
			namespace+':css:concat',
			namespace+':css:minify',

			// Notification
			namespace+':watcherNotification'
		));

		gulp.watch(sourcesScripts, gulp.series(
			// Generate new cache versions
			namespace+':cache:versions',

			// Javascripts tasks, sync with gulpfile.build.javascript.js
			// => Javascripts
			namespace+':js:move:tmp:dist',
			namespace+':js-php:move:tmp:dist',
			namespace+':lint:js',

			// => CoffeeScripts
			namespace+':coffee:move:tmp:source',
			namespace+':coffee-php:move:tmp:source',
			namespace+':coffee',
			namespace+':lint:coffee',

			// => TypeScripts
			namespace+':typescript:move:tmp:source',
			namespace+':typescript-php:move:tmp:source',
			namespace+':typescript',
			namespace+':lint:typescript',

			// => Performance optimization
			namespace+':js:concat',
			namespace+':js:minify',

			// Notification
			namespace+':watcherNotification'
		));

		gulp.watch(sourcesImages, gulp.series(
			// Generate new cache versions
			namespace+':cache:versions',

			// Sass tasks, sync with gulpfile.build.images.js
			namespace+':images:optimize',

			// Notification
			namespace+':watcherNotification'
		));
	});
};

let GulpfileBuildWatcher = function () {
	this.registerTasks = registerTasks;
};

module.exports = new GulpfileBuildWatcher();
