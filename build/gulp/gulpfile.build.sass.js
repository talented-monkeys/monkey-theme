'use strict';

// Shared
const shared = require('./gulpfile.build.shared');

// General
const gulp = require('gulp');
const rename = require('gulp-rename');
const gulpFn  = require('gulp-fn');
const notify = require("gulp-notify");
const plumber = require('gulp-plumber');
const fs = require('file-system');
const clean = require('gulp-rimraf');
const exec = require('gulp-exec');
const debug = require('gulp-debug');
const include = require('gulp-include');
const path = require('path');

// Styles
const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const cleanCSS = require('gulp-clean-css');
const concatCss = require('gulp-concat-css');
const sourcemaps = require('gulp-sourcemaps');
const gcmq = require('gulp-group-css-media-queries');

sass.compiler = require('node-sass');

const registerTasks = function ({
	namespace,
	source,
	sassLintSource,
	dist,
	distTmp,
	sassLintConfigFile,
	notifyTimeout,
	includePaths
} = {}) {

	// Define default config, if no parameters are given
	namespace			= typeof namespace			!== 'undefined' ? namespace				: 'build';
	source				= typeof source				!== 'undefined' ? source				: ['./assets/styles/**/*.s+(a|c)ss'];
	sassLintSource		= typeof sassLintSource		!== 'undefined' ? sassLintSource		: ['./assets/styles/**/*.s+(a|c)ss'];
	dist				= typeof dist				!== 'undefined' ? dist					: './dist/css';
	distTmp				= typeof distTmp			!== 'undefined' ? distTmp				: './dist/.tmp/css';
	sassLintConfigFile	= typeof sassLintConfigFile	!== 'undefined' ? sassLintConfigFile	: './config/tests/lint/sass.lint.yml';
	notifyTimeout		= typeof notifyTimeout		!== 'undefined' ? notifyTimeout			: 3;
	includePaths		= typeof includePaths		!== 'undefined' ? includePaths		: 	['./'];


	for( var i = 0; i < includePaths.length; i++){
		includePaths[i] = path.resolve((shared.projectConfig.project.path).toString(), includePaths[i].toString());
	}

	let sassConfigIncludePaths = includePaths;

	gulp.task(namespace+':clean:dist', function () {
		console.log("Clean all files in " + dist + " folder");
		let stream = gulp.src([dist, dist + '**'], { read: false, allowEmpty: true }).pipe(clean());

		return stream;
	});

	gulp.task(namespace+':clean:dist:tmp', function () {
		console.log("Clean all files in " + distTmp + " folder");
		let stream = gulp.src([distTmp, distTmp + '**'], { read: false, allowEmpty: true }).pipe(clean());

		return stream;
	});

	gulp.task(namespace+':clean', gulp.series(
		namespace+':clean:dist',
		namespace+':clean:dist:tmp'
	));

	gulp.task(namespace+':sass:generate', function (done) {
		var error = false;
		var errorMessage;
		let sassError = false;

		let options = {
			continueOnError: true, // default = false, true means don't emit error event
		};

		let stream = gulp.src(source)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':sass:generate -'}))
		.pipe(include({
			includePaths: sassConfigIncludePaths
		}))
		.pipe(sass().on('error', function(error) {
			sassError = true;
			done(error);
		}))
		.pipe(gulp.dest(distTmp))
		.on('finish', function (done) {
			if(error){
				console.log('🛑 Task "'+namespace+':sass:generate" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':sass:generate',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				if (! sassError ){
					console.log('✅ Task "'+namespace+':sass:generate" - Compiling sass to css is done!');
				}
			}
		});

		return stream;
	});

	gulp.task(namespace+':lint:sass', function () {
		var error = false;
		var errorMessage;

		let lintSassDir = shared.lintReportDir + '/sass/';

		if (!fs.existsSync(sassLintConfigFile)) {
			if (sassLintConfigFile.includes('.json')) {
				sassLintConfigFile = sassLintConfigFile.replace('.json', '.example.json');
			}
			else if (sassLintConfigFile.includes('.yml')) {
				sassLintConfigFile = sassLintConfigFile.replace('.yml', '.example.yml');
			}
		}

		if (!fs.existsSync('.' + lintSassDir)){
			fs.mkdirSync('.' + lintSassDir);
		}

		let file = fs.createWriteStream('.' + lintSassDir + 'lint.sass.report.php');
		let proof = fs.createWriteStream('.' + lintSassDir + 'lint.sass.report.proof');

		let stream = gulp.src(sassLintSource)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':lint:sass -'}))
		.pipe(sassLint({
			configFile: sassLintConfigFile,
			options: {
				formatter: 'stylish'
			}
		}))
		.pipe(sassLint.format())
		.pipe(sassLint.format(proof))
		.pipe(sassLint.failOnError())
		.pipe(sassLint({
			configFile: sassLintConfigFile,
			options: {
				formatter: 'html'
			}
		}))
		.pipe(sassLint.format(file))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':lint:sass" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':lint:sass',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				const stats = fs.statSync('.' + lintSassDir + 'lint.sass.report.proof');
				const fileSizeInBytes = stats.size;
				if ( fileSizeInBytes > 0 ){
					console.log('⚠️ Task "'+namespace+':lint:sass" - Sass linting done, found something to improve!');
					shared.devNotification({
						title: '⚠️ Task "'+namespace+':lint:sass"',
						message: "Linting done, we found some warnings",
						icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
						image: shared.gulpNotificationIconPath + 'warning.jpg',
						notificationTimeOut: 30,
						openUrl: 'http://' + shared.projectConfig.project.url.main + lintSassDir + 'lint.sass.report.php',
						actionsLabel: 'Open Report'
					});
				}
				else {
					console.log('✅ Task "'+namespace+':lint:sass" - Sass linting done, everything fine!');
				}
			}
		})
		stream.on('finish', function() {
			file.end();
			proof.end();
		});

		return stream;
	});

	gulp.task(namespace+':css:minify', function () {
		var error = false;
		var errorMessage;

		let minifiedCssSavings = 0;

		let stream = gulp.src(distTmp+'/**/*.css')
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:minify -'}))
		.pipe(sourcemaps.init())
		.pipe(gcmq())
		.pipe(cleanCSS({
			debug: true,
			level: {
				2: {
					all: true, // sets all values to `false`
					removeDuplicateRules: true // turns on removing duplicate rules
				}
			}
		},
		(details) => {
			console.log(`Minified "${details.name}" and saved: ${Math.round(details.stats.efficiency * 100)}%`);
			minifiedCssSavings = minifiedCssSavings + (Math.round(details.stats.originalSize) - Math.round(details.stats.minifiedSize));
		}))
		.pipe(sourcemaps.write('maps'))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(dist))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:minify" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:minify',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:minify" - Minified css files generated, ' + parseFloat(minifiedCssSavings / 1000).toFixed(2) + 'kb saved!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat:project', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src([distTmp + '/project/**/*.css'])
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:concat:project -'}))
		.pipe(concatCss("project.css"))
		.pipe(gulp.dest(distTmp + '/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:concat:project" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:concat:project',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:concat:project" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat:modules', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src([distTmp + '/modules/**/*.css'])
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:concat:modules -'}))
		.pipe(concatCss("modules.css"))
		.pipe(gulp.dest(distTmp + '/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:concat:modules" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:concat:modules',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:concat:modules" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat:vendor', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src([distTmp + '/vendor/**/*.css'])
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:concat:vendor -'}))
		.pipe(concatCss("vendor.css"))
		.pipe(gulp.dest(distTmp + '/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:concat:vendor" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:concat:vendor',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:concat:vendor" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat:preloader', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src([distTmp + '/preloader/**/*.css'])
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:concat:preloader -'}))
		.pipe(concatCss("preloader.css"))
		.pipe(gulp.dest(distTmp + '/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:concat:preloader" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:concat:preloader',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:concat:preloader" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat:atf', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src([distTmp + '/atf/**/*.css'])
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:concat:atf -'}))
		.pipe(concatCss("atf.css"))
		.pipe(gulp.dest(distTmp + '/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:concat:atf" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:concat:atf',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:concat:atf" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat:all', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src([distTmp + '/**/*.css'])
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':css:concat:all -'}))
		.pipe(concatCss("all.css"))
		.pipe(gulp.dest(distTmp + '/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':css:concat:all" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':css:concat:all',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':css:concat:all" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':css:concat', gulp.series(
		namespace+':css:concat:project',
		namespace+':css:concat:vendor',
		namespace+':css:concat:modules',
		namespace+':css:concat:preloader',
		namespace+':css:concat:atf',
		namespace+':css:concat:all'
	));

	gulp.task(namespace+':sass', gulp.series(
		namespace+':clean',
		namespace+':sass:generate',
		namespace+':css:concat',
		namespace+':css:minify'
	));
};

let GulpfileBuildSass = function () {
	this.registerTasks = registerTasks;
};

module.exports = new GulpfileBuildSass();
