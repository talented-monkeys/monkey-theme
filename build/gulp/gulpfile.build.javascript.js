'use strict';

// Shared
const shared = require('./gulpfile.build.shared');

// General
const gulp = require('gulp');
const exec = require('gulp-exec');
const fs = require('file-system');
const plumber = require('gulp-plumber');
const notify = require("gulp-notify");
const gulpIf = require('gulp-if');
const argv = require('yargs').argv;
const shell = require('shelljs');
const path = require('path');
const include = require('gulp-include');
const debug = require('gulp-debug');
const clean = require('gulp-rimraf');

// Scripts
const ts = require('gulp-typescript');
const coffee = require('gulp-coffee');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const coffeelint = require('gulp-coffeelint');
const eslint = require('gulp-eslint');

const registerTasks = function ({
	namespace,
	source,
	dist,
	distTmp,
	notifyTimeout,
	coffeeLintConfig,
	jsLintConfig,
	tsLintConfig,
	includePaths
} = {}) {

	// Define default config, if no parameters are given
	namespace 			= typeof namespace 			!== 'undefined' ? namespace 		: 	'build';
	source 				= typeof source 			!== 'undefined' ? source 			: 	['./assets/scripts']
	dist 				= typeof dist 				!== 'undefined' ? dist 				: 	'./dist/js';
	distTmp 			= typeof distTmp 			!== 'undefined' ? distTmp 			: 	'./dist/.tmp';
	notifyTimeout 		= typeof notifyTimeout 		!== 'undefined' ? notifyTimeout 	: 	3;
	coffeeLintConfig	= typeof coffeeLintConfig	!== 'undefined' ? coffeeLintConfig	: 	'./config/tests/lint/coffee.lint.json';
	jsLintConfig		= typeof jsLintConfig		!== 'undefined' ? jsLintConfig		: 	'./config/tests/lint/js.lint.yml';
	tsLintConfig		= typeof tsLintConfig		!== 'undefined' ? tsLintConfig		: 	'./config/tests/lint/ts.lint.yml';
	includePaths		= typeof includePaths		!== 'undefined' ? includePaths		: 	['./'];

	let jsFileFilter 			= '/**/*.js';
	let coffeeFileFilter 		= '/**/*.coffee';
	let typescriptFileFilter 	= '/**/*.ts';
	let jsPhpFileFilter 		= '/**/*.js.php';
	let coffeePhpFileFilter 	= '/**/*.coffee.php';
	let typescriptPhpFileFilter = '/**/*.ts.php';

	for( var i = 0; i < includePaths.length; i++){
	    includePaths[i] = path.resolve((shared.projectConfig.project.path).toString(), includePaths[i].toString());
	}

	let javascriptConfigIncludePaths = includePaths;

	gulp.task(namespace+':clean:dist', function () {
		console.log("Clean all files in " + dist + " folder");
		let stream = gulp.src([dist, dist + '**'], { read: false, allowEmpty: true }).pipe(clean());

		return stream;
	});

	gulp.task(namespace+':clean:dist:tmp', function () {
		console.log("Clean all files in " + distTmp + " folder");
		let stream = gulp.src([distTmp+ '/js', distTmp+ '/js' + '**'], { read: false, allowEmpty: true }).pipe(clean());

		return stream;
	});

	gulp.task(namespace+':js:move:tmp:dist', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src(source+jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:move:tmp:dist -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(gulp.dest(distTmp + '/js/dist/js'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:move:tmp:dist" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':js:move:tmp:dist',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:move:tmp:dist" - Move JS to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js-php:move:tmp:dist', function () {
		var error = false;
		var errorMessage;

		var reportOptions = {
			err: true, // default = true, false means don't write err
			stderr: true, // default = true, false means don't write stderr
			stdout: true // default = true, false means don't write stdout
		};

		let stream = gulp.src(source+jsPhpFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js-php:move:tmp:dist -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(exec('php '+ shared.projectConfig.project.path + '/build/php/build.php2asset.php '+ source +' '+ distTmp+'/js/dist/js' + ' ' + jsPhpFileFilter))
		.pipe(exec.reporter(reportOptions))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js-php:move:tmp:dist" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':js-php:move:tmp:dist',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js-php:move:tmp:dist" - Compile JavascriptsPhp to JS done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':lint:js', function() {
		var error = false;
		var errorMessage;
		const jsLintReportDir = shared.lintReportDir + 'js/';
		const jsLintFile = jsLintReportDir + 'js.lint.report.html';

		let jsLintWarningsCount = 0;
		let jsLintErrorsCount = 0;

		if (!fs.existsSync(jsLintConfig)) {
			if (jsLintConfig.includes('.json')) {
				jsLintConfig = jsLintConfig.replace('.json', '.example.json');
			}
			else if (jsLintConfig.includes('.yml')) {
				jsLintConfig = jsLintConfig.replace('.yml', '.example.yml');
			}
		}

		if (!fs.existsSync('.' + jsLintReportDir)){
			shell.mkdir('-p', '.' + jsLintReportDir);
		}

		let shouldFix = (argv.fix === undefined) ? false : true;

		function isFixed(file) {
			// Has ESLint fixed the file contents?
			return file.eslint != null && file.eslint.fixed;
		}

		let stream = gulp.src(source+jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':lint:js -'}))
		.pipe(eslint({
			globals: [
				'jQuery',
				'$',
			],
			fix: shouldFix,
			// https://eslint.org/docs/rules/
			// https://eslint.org/docs/user-guide/configuring#using-configuration-files
			configFile: jsLintConfig
		}))
		.pipe(eslint.format('stylish'))
		.pipe(eslint.format('html', fs.createWriteStream('.' + jsLintFile)))
		.pipe(eslint.result(result => {
			jsLintWarningsCount += result.warningCount;
			jsLintErrorsCount += result.errorCount;
		}))
		.pipe(gulpIf(isFixed, gulp.dest(source)))
		.on('finish', function () {
			console.log('JS linting summary:')
			console.log('› Warnings: ' + jsLintWarningsCount);
			console.log('› Errors: ' + jsLintErrorsCount);

			if(error){
				console.log('🛑 Task "'+namespace+':lint:js" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':lint:js',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				let notifyMessageLintJs;

				if ((jsLintWarningsCount > 0 && jsLintErrorsCount > 0) || jsLintErrorsCount > 0) {
					notifyMessageLintJs = 'Task "'+namespace+':lint:js" done. We found ' + jsLintErrorsCount + ' error(s)';
					if(jsLintWarningsCount > 0){
						notifyMessageLintJs += ' and ' + jsLintWarningsCount + ' warning(s)'
					}
					notifyMessageLintJs += "."
					shared.devNotification({
						title: '🛑 Error: '+namespace+':lint:js',
						message: notifyMessageLintJs,
						icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
						image: shared.gulpNotificationIconPath + 'error.jpg',
						notificationTimeOut: 30,
						openUrl: 'http://' + shared.projectConfig.project.url.main + jsLintFile,
						actionsLabel: 'Open Report'
					});
				}
				else if (jsLintWarningsCount > 0 && jsLintErrorsCount == 0) {
					notifyMessageLintJs = 'Task "'+namespace+':lint:js" done. We found ' + jsLintWarningsCount + ' warning(s).'
					shared.devNotification({
						title: '⚠️ Warning: '+namespace+':lint:js',
						message: notifyMessageLintJs,
						icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
						image: shared.gulpNotificationIconPath + 'warning.jpg',
						notificationTimeOut: 10,
						openUrl: 'http://' + shared.projectConfig.project.url.main + jsLintFile,
						actionsLabel: 'Open Report'
					});
				}
				else {
					console.log('✅ Task "'+namespace+':lint:js" - Javascript linting done, everything fine!');
				}
			}
		});

		return stream;
	});

	gulp.task(namespace+':coffee:move:tmp:source', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src(source+coffeeFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':coffee:move:tmp:source -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(gulp.dest(distTmp + '/js/source/coffee'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':coffee:move:tmp:source" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':coffee:move:tmp:source',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':coffee:move:tmp:source" - Move CoffeeScript to temporary dist done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':coffee-php:move:tmp:source', function () {
		var error = false;
		var errorMessage;

		var reportOptions = {
			err: true, // default = true, false means don't write err
			stderr: true, // default = true, false means don't write stderr
			stdout: true // default = true, false means don't write stdout
		};

		let stream = gulp.src(source+coffeePhpFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':coffee-php:move:tmp:source -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(exec('php '+ shared.projectConfig.project.path + '/build/php/build.php2asset.php '+ source +' '+ distTmp+'/js/source/coffee' + ' ' + coffeePhpFileFilter))
		.pipe(exec.reporter(reportOptions))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':coffee-php:move:tmp:source" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':coffee-php:move:tmp:source',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':coffee-php:move:tmp:source" - Compile CoffeeScriptPhp to CoffeeScript done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':coffee', async function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/source/coffee'+coffeeFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':coffee -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(coffee({bare: true}))
		.pipe(gulp.dest(distTmp + '/js/dist/js'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':coffee" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':coffee"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':coffee" - Compile CoffeeScript to JS done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':lint:coffee', function () {
		var error = false;
		var errorMessage;

		let coffeeLintWarningsCount = 0;
		let coffeeLintErrorsCount = 0;

		const coffeeScriptLintReportDir = shared.lintReportDir + 'coffee/';
		const coffeeScriptLintReportFile = coffeeScriptLintReportDir + 'coffescript.lint.report.php';
		const coffeeScriptLintImportFileName = 'coffescript.lint.report.import.php'
		const coffeeScriptLintImportFile = coffeeScriptLintReportDir + coffeeScriptLintImportFileName;
		let coffeeScriptLintConfig = coffeeLintConfig;

		if (!fs.existsSync(coffeeScriptLintConfig)) {
			if (coffeeScriptLintConfig.includes('.json')) {
				coffeeScriptLintConfig = coffeeScriptLintConfig.replace('.json', '.example.json');
			}
			else if (coffeeScriptLintConfig.includes('.yml')) {
				coffeeScriptLintConfig = coffeeScriptLintConfig.replace('.yml', '.example.yml');
			}
		}

		if (!fs.existsSync('.' + coffeeScriptLintReportDir)){
			shell.mkdir('-p', '.' + coffeeScriptLintReportDir);
		}

		fs.copyFileSync(path.resolve(__dirname,'./assets/templates/reports/gulp.report.template.php'), '.' + coffeeScriptLintReportFile);
		fs.copyFileSync(path.resolve(__dirname,'./assets/templates/reports/gulp.report.template.css'), '.' + coffeeScriptLintReportDir + 'gulp.report.template.css');

		fs.readFile('.' + coffeeScriptLintReportFile, 'utf8', function (err,data) {
			if (err) {
				return console.log(err);
			}

			data = data.replace(/%replace_title%/g, 'CoffeScript Linting Report');
			data = data.replace(/%replace_report_result%/g, '<?php include("' + coffeeScriptLintImportFileName + '"); ?>');

			fs.writeFile('.' + coffeeScriptLintReportFile, data, 'utf8', function (err) {
				if (err) return console.log(err);
			});
		});

		let writeStream = fs.createWriteStream('.' + coffeeScriptLintImportFile);
		let coffeLintResults = false;
		let htmlCode;

		let coffeeLintReporter = (function() {

			function coffeeLintNotifyReporter(errorReport) {
				this.errorReport = errorReport;
			}

			coffeeLintNotifyReporter.prototype.publish = function() {
				let lintSummary = this.errorReport.getSummary();

				if (lintSummary.warningCount > 0 || lintSummary.errorCount > 0) {

					// Getting linting results
					for (var file in this.errorReport.paths) {
						//console.log('Filename: ' + file);

						htmlCode = '<li>';
						htmlCode +=   '<div class="file-name">' + file + '</div>';

						var results = this.errorReport.paths[file];
						for (var result in results) {
							// skip loop if the property is from prototype
							if(!results.hasOwnProperty(result)) continue;

							var report = results[result];
							htmlCode += '<div class="report-entry">';
							htmlCode +=   '<span class="line">line ' + report['lineNumber'] + '</span>';
							htmlCode +=   '<span class="type ' + report['level'] + '">' + report['level'] + '</span>';
							htmlCode +=   '<span class="message">' + report['message'] + '</span>';
							if(report['context'] != undefined){
								htmlCode += '<span class="context">' + report['context'] + '</span>';
							}
							htmlCode +=   '<span class="rule">(' + report['rule'] + ')</span>';
							htmlCode += '</div>';
						}

						htmlCode += '</li>';

						writeStream.write(htmlCode);

						htmlCode = '';
					}

					coffeeLintWarningsCount += lintSummary.warningCount;
					coffeeLintErrorsCount += lintSummary.errorCount;

					return coffeLintResults = true;
				}
			}

			return coffeeLintNotifyReporter;
		})();

		let stream = gulp.src(source+coffeeFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(coffeelint({
			optFile: coffeeScriptLintConfig
		}))
		.pipe(coffeelint.reporter())
		.pipe(coffeelint.reporter(coffeeLintReporter))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':lint:coffee" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':lint:coffee"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				if (coffeLintResults) {
					console.log('Coffee Script linting summary:')
					console.log('› Warnings: ' + coffeeLintWarningsCount);
					console.log('› Errors: ' + coffeeLintErrorsCount);

					let notifyMessageLintJs;

					if ((coffeeLintWarningsCount > 0 && coffeeLintErrorsCount > 0) || coffeeLintErrorsCount > 0) {
						notifyMessageLintJs = 'lint:coffee done. We found ' + coffeeLintErrorsCount + ' error(s)';
						if(coffeeLintWarningsCount > 0){
							notifyMessageLintJs += ' and ' + coffeeLintWarningsCount + ' warning(s)'
						}
						notifyMessageLintJs += "."
						shared.devNotification({
							title: '🛑 Error on lint CoffeeScripts',
							message: notifyMessageLintJs,
							icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
							image: shared.gulpNotificationIconPath + 'error.jpg',
							notificationTimeOut: 30,
							openUrl: 'http://' + shared.projectConfig.project.url.main + coffeeScriptLintReportFile,
							actionsLabel: 'Open Report'
						});
					}
					else if (coffeeLintWarningsCount > 0 && coffeeLintErrorsCount == 0) {
						notifyMessageLintJs = 'Task "'+namespace+':lint:coffee" done. We found ' + coffeeLintWarningsCount + ' warning(s).'
						shared.devNotification({
							title: '⚠️ Warning on lint CoffeeScripts',
							message: notifyMessageLintJs,
							icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
							image: shared.gulpNotificationIconPath + 'warning.jpg',
							notificationTimeOut: 10,
							openUrl: 'http://' + shared.projectConfig.project.url.main + coffeeScriptLintReportFile,
							actionsLabel: 'Open Report'
						});
					}
				}
				else {
					htmlCode = '<li> Good Job! No linting warnings and errors! </li>';

					writeStream.write(htmlCode);

					console.log('✅ Task "'+namespace+':lint:coffee" - CoffeeScript linting done, everything fine!');
				}
			}

			// close the stream
			writeStream.end();

			// the finish event is emitted when all data has been flushed from the stream
			writeStream.on('finish', () => {
			});
		});

		return stream;
	});

	gulp.task(namespace+':typescript:move:tmp:source', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src(source+typescriptFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':typescript:move:tmp:source -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(gulp.dest(distTmp + '/js/source/typescript'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':typescript:move:tmp:source" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':typescript:move:tmp:source"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':typescript:move:tmp:source" - Move TypeScript to temporary directory done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':typescript-php:move:tmp:source', function () {
		var error = false;
		var errorMessage;

		var reportOptions = {
			err: true, // default = true, false means don't write err
			stderr: true, // default = true, false means don't write stderr
			stdout: true // default = true, false means don't write stdout
		};

		let stream = gulp.src(source+typescriptPhpFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':typescript-php:move:tmp:source -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(exec('php '+ shared.projectConfig.project.path + '/build/php/build.php2asset.php '+ source +' '+ distTmp+'/js/source/typescript' + ' ' + typescriptPhpFileFilter))
		.pipe(exec.reporter(reportOptions))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':typescript-php:move:tmp:source" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':typescript-php:move:tmp:source"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':typescript-php:move:tmp:source" - Compile TypeScriptPhp to TypeScriptPhp done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':typescript', function () {
		var error = false;
		var errorMessage;

		var stream = gulp.src(distTmp + '/js/source/typescript'+typescriptFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':typescript -'}))
		.pipe(include({
			includePaths: [
				javascriptConfigIncludePaths
			]
		}))
		.pipe(ts({
			noImplicitAny: true,
			target: 'ES6'
		}))
		.pipe(gulp.dest(distTmp + '/js/dist/js'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':typescript" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':typescript"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':typescript" - Compile TypeScript to JS done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':lint:typescript', function() {
		var error = false;
		var errorMessage;

		const tsLintReportDir = shared.lintReportDir + 'typescript/';
		const tsLintFile = tsLintReportDir + 'ts.lint.report.html';

		if (!fs.existsSync(tsLintConfig)) {
			if (tsLintConfig.includes('.tson')) {
				tsLintConfig = tsLintConfig.replace('.json', '.example.json');
			}
			else if (tsLintConfig.includes('.yml')) {
				tsLintConfig = tsLintConfig.replace('.yml', '.example.yml');
			}
		}

		if (!fs.existsSync('.' + tsLintReportDir)){
			shell.mkdir('-p', '.' + tsLintReportDir);
		}

		let tsLintWarningsCount = 0;
		let tsLintErrorsCount = 0;

		let shouldFix = (argv.fix === undefined) ? false : true;

		function isFixed(file) {
			// Has ESLint fixed the file contents?
			return file.eslint != null && file.eslint.fixed;
		}

		var stream = gulp.src(source+typescriptFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':lint:typescript -'}))
		.pipe(eslint({
			parser: "@typescript-eslint/parser",
			fix: shouldFix,
			// https://github.com/typescript-eslint/typescript-eslint
			// https://www.npmjs.com/package/@typescript-eslint/eslint-plugin
			configFile: tsLintConfig
		}))
		.pipe(eslint.format('stylish'))
		.pipe(eslint.format('html', fs.createWriteStream('.' + tsLintFile)))
		.pipe(eslint.result(result => {
			tsLintWarningsCount += result.warningCount;
			tsLintErrorsCount += result.errorCount;
		}))
		.pipe(gulpIf(isFixed, gulp.dest('./src/js')))
		.on('finish', function () {
			console.log('TS linting summary:')
			console.log('› Warnings: ' + tsLintWarningsCount);
			console.log('› Errors: ' + tsLintErrorsCount);

			if(error){
				console.log('🛑 Task "'+namespace+':lint:typescript" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':lint:typescript"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				let notifyMessageLintTs;

				if ((tsLintWarningsCount > 0 && tsLintErrorsCount > 0) || tsLintErrorsCount > 0) {
					notifyMessageLintTs = 'Task "'+namespace+':lint:ts" done. We found ' + tsLintErrorsCount + ' error(s)';
					if(tsLintWarningsCount > 0){
						notifyMessageLintTs += ' and ' + tsLintWarningsCount + ' warning(s)'
					}
					notifyMessageLintTs += "."
					shared.devNotification({
						title: '🛑 Error: '+namespace+':lint:ts',
						message: notifyMessageLintTs,
						icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
						image: shared.gulpNotificationIconPath + 'error.jpg',
						notificationTimeOut: 30,
						openUrl: 'http://' + shared.projectConfig.project.url.main + tsLintFile,
						actionsLabel: 'Open Report'
					});
				}
				else if (tsLintWarningsCount > 0 && tsLintErrorsCount == 0) {
					notifyMessageLintTs = 'Task "'+namespace+':lint:ts" done. We found ' + tsLintWarningsCount + ' warning(s).'
					shared.devNotification({
						title: '⚠️ Warning: '+namespace+':lint:ts',
						message: notifyMessageLintTs,
						icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
						image: shared.gulpNotificationIconPath + 'warning.jpg',
						notificationTimeOut: 10,
						openUrl: 'http://' + shared.projectConfig.project.url.main + tsLintFile,
						actionsLabel: 'Open Report'
					});
				}
				else {
					console.log('✅ Task "'+namespace+':lint:ts" - Javascript linting done, everything fine!');
				}
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat:project', function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js/project/' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:concat:project -'}))
		.pipe(concat('project.js'))
		.pipe(gulp.dest(distTmp + '/js/dist/js/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:concat:project" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':js:concat:project"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:concat:project" - Concatting project scripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat:modules', function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js/modules/' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:concat:modules -'}))
		.pipe(concat('modules.js'))
		.pipe(gulp.dest(distTmp + '/js/dist/js/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:concat:modules" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':js:concat:modules"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:concat:modules" - Concatting modules scripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat:vendor', function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js/vendor/' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:concat:vendor -'}))
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest(distTmp + '/js/dist/js/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:concat:vendor" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':js:concat:vendor"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:concat:vendor" - Concatting vendor scripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat:preloader', function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js/preloader/' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:concat:preloader -'}))
		.pipe(concat('preloader.js'))
		.pipe(gulp.dest(distTmp + '/js/dist/js/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:concat:preloader" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':js:concat:preloader"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:concat:preloader" - Concatting preloader scripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat:atf', function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js/atf/' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:concat:atf -'}))
		.pipe(concat('atf.js'))
		.pipe(gulp.dest(distTmp + '/js/dist/js/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:concat:atf" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':js:concat:atf"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:concat:atf" - Concatting atf scripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat:all', function() {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js/**/' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:concat:all -'}))
		.pipe(concat('all.js'))
		.pipe(gulp.dest(distTmp + '/js/dist/js/combined'))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:concat:all" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: "'+namespace+':js:concat:all"',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:concat:all" - Concatting all scripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js:concat', gulp.series(
		namespace+':js:concat:project',
		namespace+':js:concat:vendor',
		namespace+':js:concat:modules',
		namespace+':js:concat:preloader',
		namespace+':js:concat:atf',
		namespace+':js:concat:all'
	));

	gulp.task(namespace+':js:minify', function () {
		var error = false;
		var errorMessage;

		let stream = gulp.src(distTmp + '/js/dist/js' + jsFileFilter)
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = error.message;
		}}))
		.pipe(debug({title: namespace+':js:minify -'}))
		.pipe(minify({
			noSource: true,
			ext:{
				min:'.min.js'
			},
		}))
		.pipe(gulp.dest(dist))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':js:minify" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':js:minify',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':js:minify" - Minifying javascripts is done!');
			}
		});

		return stream;
	});

	gulp.task(namespace+':js', gulp.series(
		// Prerequisites
		namespace+':clean:dist',
		namespace+':clean:dist:tmp',

		// Javascripts
		namespace+':js:move:tmp:dist',
		namespace+':js-php:move:tmp:dist',
		namespace+':lint:js',

		// CoffeeScripts
		namespace+':coffee:move:tmp:source',
		namespace+':coffee-php:move:tmp:source',
		namespace+':coffee',
		namespace+':lint:coffee',

		// TypeScripts
		namespace+':typescript:move:tmp:source',
		namespace+':typescript-php:move:tmp:source',
		namespace+':typescript',
		namespace+':lint:typescript',

		// Performance optimization
		namespace+':js:concat',
		namespace+':js:minify'
	));
};

let GulpfileBuildJavascript = function () {
	this.registerTasks = registerTasks;
};

module.exports = new GulpfileBuildJavascript();
