const browserSync = require('browser-sync').create();
const notifier = require('node-notifier');
const opn = require('opn');
const read = require('read-yaml');
const config = read.sync('./config/config.yml');

let httpsActivated = false;
let namespace;

if(config.project.ssl.https == "enable"){
	httpsActivated = true;
}

module.exports = {
	projectConfig: config,
	lintReportDir: '/reports/lint/',
	gulpNotificationIconPath: __dirname+'/assets/images/notifications/',
	browserSyncConfigLogPrefix: config.project.name,
	browserSyncConfigHost: config.project.url.main,
	browserSyncConfigPort: 3060,
	browserSyncConfigOpen: false,
	browserSyncConfigNotify: false,
	browserSyncConfigGhost: false,
	browserSyncConfigInjectChanges: true,
	browserSyncConfigHttps: httpsActivated,
	browserSyncConfigHttpsKey: config.project.path + '/config/ssl/' + config.environment + '.' + config.project.url.main + '.key',
	browserSyncConfigHttpsCert: config.project.path + '/config/ssl/' + config.environment + '.' + config.project.url.main + '.crt',
	devCloseNotifications: function () {
		if ( config.environment == "local" ){
			notifier.notify({
				remove: 612 // to remove all group ID
			});
		}
	},
	devNotification: function ( {title, message, icon, image, notificationTimeOut, actionsLabel, openUrl } = {} ) {
		if ( config.environment == "local" ){
			notifier.notify({
				title: title,
				message: message,
				icon: icon,
				contentImage: image,
				timeout: notificationTimeOut,
				open: openUrl,
				actions: actionsLabel,
				id: 612
			}, function(err, response) {
				if(response == "activate"){
					opn(openUrl);
				}
			});
		}
	}
}
