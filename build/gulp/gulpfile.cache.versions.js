'use strict';

// Shared
const shared = require('./gulpfile.build.shared');

// General
const gulp = require('gulp');
const fs = require('file-system');
const shell = require('shelljs');
const exec = require('gulp-exec');
const plumber = require('gulp-plumber');

const registerTasks = function ({	namespace, packageJsonFile, dist, notifyTimeout } = {}) {

	// Define default config, if no parameters are given
	namespace 		= typeof namespace 			!== 'undefined' ? namespace 		: 'build';
	dist 			= typeof dist 				!== 'undefined' ? dist 				: './config/cache/version';
	notifyTimeout 	= typeof notifyTimeout 		!== 'undefined' ? notifyTimeout 	: 3;
	packageJsonFile	= typeof packageJsonFile	!== 'undefined' ? packageJsonFile	: '../../package.json';

	// Cache: Version
	const pjson = require(packageJsonFile);

	gulp.task(namespace+':cache:versions', function() {
		var error = false;
		var errorMessage;

		const cacheVersionPath = dist;

		if (!fs.existsSync(cacheVersionPath)){
			shell.mkdir('-p', cacheVersionPath);
		}

		var reportOptions = {
			err: true,
			stderr: true,
			stdout: true
		};

		const today = new Date();
		const yyyy = today.getFullYear();
		let dd = today.getDate();
		let mm = today.getMonth()+1;

		if(dd<10){ dd='0'+dd }
		if(mm<10){ mm='0'+mm }

		const dateVersion = yyyy+mm+dd;

		var stream = gulp.src('./package.json', {read: false})
		.pipe(plumber({errorHandler: function() {
			error = true;
			errorMessage = '<%= error.message %>';
		}}))
		.pipe(exec('git rev-parse --short HEAD > '+ cacheVersionPath +'/.hash-short-version'))
		.pipe(exec('git rev-parse HEAD > '+ cacheVersionPath +'/.hash-version'))
		.pipe(exec('echo ' + pjson.version + ' > '+ cacheVersionPath +'/.project-version'))
		.pipe(exec('echo ' + Math.floor(Date.now() / 1000) + ' > '+ cacheVersionPath +'/.timestamp-version'))
		.pipe(exec('echo ' + dateVersion + ' > '+ cacheVersionPath +'/.date-version'))
		.pipe(exec.reporter(reportOptions))
		.on('finish', function () {
			if(error){
				console.log('🛑 Task "'+namespace+':cache:versions" errored - Notification send');
				shared.devNotification({
					title: '🛑 Error: '+namespace+':cache:versions',
					message: errorMessage,
					icon:  shared.gulpNotificationIconPath + 'monkey-theme.jpg',
					image: shared.gulpNotificationIconPath + 'error.jpg',
					notificationTimeOut: 30,
					openUrl: '#',
					actionsLabel: '#'
				});
			}
			else{
				console.log('✅ Task "'+namespace+':cache:versions" done!');
			}
		});

		return stream;
	});
}

let GulpfileBuildCacheVersions = function () {
	this.registerTasks = registerTasks;
};

module.exports = new GulpfileBuildCacheVersions();
