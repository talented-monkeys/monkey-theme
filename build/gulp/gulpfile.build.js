'use strict';

// Shared & Tasks
const shared = require('./gulpfile.build.shared');
const sassTasks = require('./gulpfile.build.sass');
const javascriptTasks = require('./gulpfile.build.javascript');
const imagesTasks = require('./gulpfile.build.images');
const cacheVersionsTasks = require('./gulpfile.cache.versions');

// General
const gulp = require('gulp');

const registerTasks = function ( {
	namespace
} = {} ) {

	// Define default config, if no parameters are given
	namespace = typeof namespace !== 'undefined' ? namespace : 'build';

	gulp.task(namespace+':assets:build',
		gulp.series(
			// Generate new cache versions
			namespace+':cache:versions',

			// Sass tasks, sync with gulpfile.build.sass.js
			// => Sass
			namespace+':clean',
			namespace+':sass:generate',
			namespace+':lint:sass',
			namespace+':css:concat',
			namespace+':css:minify',

			// Javascripts tasks, sync with gulpfile.build.javascript.js
			// => Javascripts
			namespace+':js:move:tmp:dist',
			namespace+':js-php:move:tmp:dist',
			namespace+':lint:js',

			// => CoffeeScripts
			namespace+':coffee:move:tmp:source',
			namespace+':coffee-php:move:tmp:source',
			namespace+':coffee',
			namespace+':lint:coffee',

			// => TypeScripts
			namespace+':typescript:move:tmp:source',
			namespace+':typescript-php:move:tmp:source',
			namespace+':typescript',
			namespace+':lint:typescript',

			// => Performance optimization
			namespace+':js:concat',
			namespace+':js:minify',

			// Sass tasks, sync with gulpfile.build.images.js
			namespace+':images:optimize'
		)
	);
};

let GulpfileBuild = function () {
	this.registerTasks = registerTasks;
};

module.exports = new GulpfileBuild();
