<?php
	/**
	 *  Author: Tobias Feld
	 *  
	 *  Generate coffee file through a php file
	**/

	$source = $argv[1];
	$dist = $argv[2];	
	$file_filter = str_replace('/**/*.', '', $argv[3]);
	$source_files = array();

	$di = new RecursiveDirectoryIterator($source);
	foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
			// check if it is the correct file type
			if (strpos($filename, $file_filter) !== false) {
			    $source_files[] = $filename;
			}
	}

	// Create files with compiled php
	foreach ($source_files as $key => $value) {

		$compiled_file = str_replace(".php", "", $value); // new sass filename

		$source_file = $value; //$dist.str_replace('./', '', $value);	// source file
		$target_file = $dist.str_replace($source, '', $compiled_file);	// target file
		$target_dist = str_replace('/'.basename($target_file), '', $target_file);	// target file

		if(!is_dir($target_dist)){
	    //Directory does not exist, so lets create it.
	    mkdir($target_dist, 0755, true);
		}

		compileFile($source_file, $target_file);
	}

	function compileFile($source, $target) {

		ob_start();

		include($source);
		file_put_contents($target, ob_get_clean());

		if (ob_get_contents()) {
			ob_end_clean();
		}
	}

?>