// Register gulp sass tasks
// commands:
// » gulp build:sass « One time css generation
// » gulp build:lint:sass « Linting your sass files to following development rules of sass
// » gulp build:sass:minify « Minfies the generated css files
// » gulp build:sass:concat « combines the generated css files into one file
// » gulp build:sass:concat:vendor « combines the generated css files into one file [VENDOR FOLDER]
require('./build/gulp/gulpfile.build.sass').registerTasks();

// Register gulp js tasks
// commands:
// » gulp build:js « One time javascript generation
// » gulp build:lint:coffee « Linting your coffee files to following development rules
// » gulp build:lint:js « Linting your coffee files to following development rules
// » gulp build:js:minify « Minfies the generated js files
// » gulp build:js:concat « combines the generated js files into one file
// » gulp build:js:concat:vendor « combines the generated js files into one file [VENDOR FOLDER]
require('./build/gulp/gulpfile.build.javascript').registerTasks();

// Register gulp image tasks
// commands:
// » gulp build:images:optimize « One time image optimization
require('./build/gulp/gulpfile.build.images').registerTasks();

// Register gulp cache versions task
// commands:
// » gulp build:cache:versions « Generates different version files
require('./build/gulp/gulpfile.cache.versions').registerTasks();

// Register gulp watch task
// commands:
// » gulp build:assets:watch « Watcher for ongoing development
require('./build/gulp/gulpfile.build.assets.watch').registerTasks();

// Register gulp build task
// commands:
// » gulp build:assets:build « One time assets generation
require('./build/gulp/gulpfile.build').registerTasks();
