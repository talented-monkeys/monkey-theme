<?php

/*
* Theme functions
*/

// Remove unused generated css and js files
include get_template_directory() . "/config/wordpress/mt.parent.wp-remove-unused-css-js.functions.php";

if (SMTP_HOST && SMTP_USER && SMTP_PASS){
	// Enable SMTP mailing via external smtp server
	include get_template_directory() . "/config/wordpress/mt.parent.wp-enable-external-smtp-mailer.functions.php";
}

if (WP_COMMENTS == "disable"){
	// Disable comments if defined in wp-config
	include get_template_directory() . "/config/wordpress/mt.parent.wp-disable-comments.functions.php";
}

if (WP_BLOG == "disable"){
	// Disable wordpress blog features if defined in wp-config
	include get_template_directory() . "/config/wordpress/mt.parent.wp-disable-blog.functions.php";

	// Disable wordpress emojie features if defined in wp-config
	include get_template_directory() . "/config/wordpress/mt.parent.wp-disable-emojies.functions.php";
}

if (PROJECT_ENVIRONMENT == "local"){
	// Activate GULP BrowserSync on dev environment
	include get_template_directory() . "/config/wordpress/mt.parent.wp-build-gulp-browser-sync.functions.php";
}

// Adding wordpress menu item for monkey-theme
include get_template_directory() . "/config/wordpress/mt.parent.wp-setup-monkey-theme-menu-entry.functions.php";

// Reorder the wordpress menu items
include get_template_directory() . "/config/wordpress/mt.parent.wp-config-menu-order.functions.php";

// Getting versions for cache controle from the gulp tasks
include get_template_directory() . "/config/wordpress/mt.parent.wp-config-get-cache-versions.functions.php";
// Usage of the cache control const:
// - MT_CACHE_VERSION_TIMESTAMP 	=> Timestamp of the the last assets build gulp task
// - MT_CACHE_VERSION_DATE 				=> Date (format yyyymmdd) of the the last assets build gulp task
// - MT_CACHE_VERSION_PROJECT 		=> Version number of the package.json
// - MT_CACHE_VERSION_HASH 				=> Current git commit hash
// - MT_CACHE_VERSION_HASH_SHORT 	=> Current git commit hash (short version)

// Include wordpress child theme assets enqueue scripts
include get_template_directory() . '/config/wordpress/mt.parent.wp-build-enqueue-assets.functions.php';
